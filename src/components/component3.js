import React from 'react'

export default function component3() {
  return (
    <div style={c3Style}>
      <h1>Component 3</h1>
        <p>
          Id veniam reprehenderit ex consequat et occaecat adipisicing dolore 
          nostrud. Ex sunt deserunt voluptate ullamco do sit occaecat labore. 
          Duis ex cupidatat minim Lorem tempor. Ullamco adipisicing eu velit 
          laboris voluptate laboris mollit.
        </p>
    </div>
  )
}

const c3Style = {
  background: 'blue',
  color: 'red',
  padding: '1.5rem 1.5rem 5rem 1.5rem'
}
