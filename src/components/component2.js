import React, { Component } from 'react';
import {Spring} from 'react-spring/renderprops';

export default class component2 extends Component {
  render() {
    return (
      <Spring
      from={{opacity: 0}}
      to={{opacity: 1}}
      config={{delay: 1000, duration: 1000}}
    >
      {props => (
        <div style={props}>
          <div style={c2Style}>
            <h1>Component 2</h1>
            <p>
              Id veniam reprehenderit ex consequat et occaecat adipisicing dolore 
              nostrud. Ex sunt deserunt voluptate ullamco do sit occaecat labore. 
              Duis ex cupidatat minim Lorem tempor. Ullamco adipisicing eu velit 
              laboris voluptate laboris mollit.
            </p>
            <button style={btn} onClick={this.props.toggle}>Toggle Component 3</button>
          </div>
        </div>
      )} 
    </Spring>
    )
  }
}

const c2Style = {
  background: 'red',
  color: 'white',
  padding: '1.5rem'
}

const btn = {
  background: '#333',
  color: '#fff',
  padding: '1rem 2rem',
  border: 'none',
  textTransformation: 'uppercase',
  margin: '15px 0'
}