import React, { Component } from 'react';
import {Transition, animated} from 'react-spring/renderprops';
import './App.css';
import Component1 from './components/component1';
import Component2 from './components/component2';
import Component3 from './components/component3';

export default class App extends Component {
  state = {
    showComponent3: false
  }

  toggle = e => {
    // testing
    // console.log(123);
    this.setState({showComponent3: !this.state.showComponent3})
  }

  render() {
    return (
      <div>
        <Component1 />
        <Component2 toggle={this.toggle}/>
        {/* fading in Component 3 by toggling button in Component 2*/}
        <Transition
          native
          items={this.state.showComponent3}
          from={{opacity: 0}}
          enter={{opacity: 1}}
          leave={{ opacity: 0}}
        >
          {show => show && (props => (
            <animated.div style={props}>
              <Component3/>
            </animated.div>
          ))}
        </Transition>
      </div>
    )
  }
}
